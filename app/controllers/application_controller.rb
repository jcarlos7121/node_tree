require './config/environment'

class ApplicationController < Sinatra::Base
  before do
    content_type :json
  end

  get "/common_ancestor" do
    report = NodeReport.run!(params)

    report.to_json
  end
end
