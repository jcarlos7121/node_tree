class NodeReport < ActiveInteraction::Base
  integer :a
  integer :b

  def node_report
    Arel::Table.new(
      Arel::Nodes::NamedFunction.new(
        'get_node_report',
        [a, b]
      ).to_sql
    ).project(Arel.star)
  end

  def result_set
    @result_set ||= ActiveRecord::Base.connection.exec_query(
      node_report.to_sql.gsub!('"', '')
    )
  end

  def execute
    result_set.entries
  end
end
