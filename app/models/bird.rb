class Bird < ActiveRecord::Base
  # Search for birds inside the tree
  scope :tree_node, ->(node) do
    joins(:node).where('(nodes.path <@ ? AND nodes.id != ?)', node.id.to_s, node.id.to_s)
  end

  belongs_to :node
end
