# == Schema Information
#
# Table name: nodes
#
#  id         :bigint           not null, primary key
#  parent_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  path       :ltree
#
class Node < ActiveRecord::Base
  has_ltree_hierarchy

  has_many :birds # This only lists the inmediate birds of the node by node_id foreign_key.

  # This one lists all the child birds of all the descendants
  def child_birds
    Bird.tree_node(self)
  end
end
