# Node Tree

## Prerequesites

To run the server in development mode, you'll need:

* ruby interpreter version according to `.ruby-version`. Using [rvm] or [rbenv]
is advisable
* PostgreSQL database version 10, using the [Postgres.app] is the
simplest way to get it running.

## Setting up

* `bundle install` to get all required gems
* `bundle exec rake db:create` to create the database
* `bundle exec rake db:migrate` to create the database
* `bundle exec rake import:nodes` to import the nodes and birds

## Running

`bundle exec rackup`

This will run the Sintra process on <http://localhost:9292>

[rvm]: http://rvm.io
[rbenv]: http://rbenv.org
[Postgres.app]: http://postgresapp.com/

## Continous Integration

`bundle exec rspec` # Runs all tests

# To visualize the internal SQL function for node lookup

Import files

./spec/fixtures/birds.csv
./spec/fixtures/nodes.csv

Open SQL file

./internal.sql

Migration for the internal SQL Node Report function

./db/migrate/20220826001529_add_node_report_function.rb

Specs definition

./spec/application_controller_spec.rb
