class CreateNodes < ActiveRecord::Migration[6.1]
  def change
    create_table :nodes do |t|
      t.integer :parent_id, index: true
      t.timestamps null: false
    end
  end
end
