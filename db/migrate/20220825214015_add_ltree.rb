class AddLtree < ActiveRecord::Migration[6.1]
  def up
    enable_extension 'ltree'
  end

  def down
    disable_extension 'ltree'
  end
end
