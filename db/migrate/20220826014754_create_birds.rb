class CreateBirds < ActiveRecord::Migration[5.2]
  def change
    create_table :birds do |t|
      t.string :name
      t.belongs_to :node, index: true
      t.timestamps null: false
    end
  end
end
