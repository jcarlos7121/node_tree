class AddPathToNodes < ActiveRecord::Migration[6.1]
  def change
    add_column :nodes, :path, :ltree
    add_index :nodes, :path, using: :gist
  end
end
