# SQL FUNCTION TO GENERATE REPORT ON DEMAND
CREATE OR REPLACE FUNCTION get_node_report(a int, b int)
  RETURNS TABLE (root ltree, lowest_common_ancestor ltree, depth int) AS
$func$
BEGIN
  RETURN QUERY
  SELECT
    subpath(first_nodes.path, 0, 1) as root,
    lca(first_nodes.path, second_nodes.path) AS lowest_common_ancestor,
    nlevel(GREATEST(first_nodes.path, second_nodes.path)) AS depth
    FROM
      nodes AS first_nodes,
      nodes AS second_nodes
      WHERE
        first_nodes.id = a AND second_nodes.id = b;
END
$func$ LANGUAGE plpgsql;

# SQL QUERY TO GET THE NODE REPORT
SELECT
  subpath(first_nodes.path, 0, 1) as root,
  lca(first_nodes.path, second_nodes.path) AS lowest_common_ancestor,
  nlevel(GREATEST(first_nodes.path, second_nodes.path)) AS depth
  FROM
    nodes AS first_nodes,
    nodes AS second_nodes
    WHERE
      first_nodes.id = 2191348 AND second_nodes.id = 2191347;
