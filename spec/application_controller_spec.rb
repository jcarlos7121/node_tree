require_relative "spec_helper"
require 'csv'

def app
  ApplicationController
end

describe ApplicationController do
  before do
    CSV.foreach('spec/fixtures/nodes.csv', headers: true) do |row|
      Node.create! row.to_hash
    end

    CSV.foreach('spec/fixtures/birds.csv', headers: true) do |row|
      Bird.create! row.to_hash
    end

    Node.find_each(&:commit_path)
    Node.find_each(&:commit_path)
    Node.find_each(&:commit_path)
  end

  context 'a=2191348&b=2191347' do
    subject { JSON.parse(last_response.body).first }

    it "responds with a node report" do
      get '/common_ancestor?a=2191348&b=2191347'

      expect(last_response.status).to eq(200)
      expect(subject['lowest_common_ancestor']).to eq '456841.2191349'
      expect(subject['depth']).to eq 4
      expect(subject['root']).to eq '456841'
    end
  end

  context 'a=2176287&b=2176289' do
    subject { JSON.parse(last_response.body).first }

    it "responds with a node report" do
      get '/common_ancestor?a=2176287&b=2176289'

      expect(last_response.status).to eq(200)
      expect(subject['lowest_common_ancestor']).to eq '1490994.2176286.2176285'
      expect(subject['depth']).to eq 5
      expect(subject['root']).to eq '1490994'
    end
  end

  context 'a=2514874&b=2514881' do
    subject { JSON.parse(last_response.body).first }

    it "responds with a node report" do
      get '/common_ancestor?a=2514874&b=2514881'

      expect(last_response.status).to eq(200)
      expect(subject['lowest_common_ancestor']).to eq '1558332.2514873.2514875'
      expect(subject['depth']).to eq 5
      expect(subject['root']).to eq '1558332'
    end
  end

  context 'birds' do
    subject { Node.find 456841 }

    it "expects to return all child birds" do
      expect(subject.child_birds.count).to eq 3
      expect(subject.child_birds.pluck(:node_id)).to include(2191348, 2191347, 2191354)
    end
  end
end
