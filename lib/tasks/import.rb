require 'csv'

namespace :import do
  desc "Import data from a CSV file"
  task :nodes do
    CSV.foreach('spec/fixtures/nodes.csv', headers: true) do |row|
      print '.'
      Node.create! row.to_hash
    end

    puts "\nImported #{Node.count} nodes"

    CSV.foreach('spec/fixtures/birds.csv', headers: true) do |row|
      print '.'
      Bird.create! row.to_hash
    end

    puts "\nImported #{Bird.count} birds"

    # Refactor ltree path indexing did it this way just as a POC
    Node.find_each(&:commit_path)
    Node.find_each(&:commit_path)
    Node.find_each(&:commit_path)
  end

  desc "Destroy all data"
  task :destroy_all do
    Node.destroy_all
  end
end
